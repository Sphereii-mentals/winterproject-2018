﻿using UnityEngine;
using System;
    class BlockPreloader : Block
    {

    public override void Init()
    {
        base.Init();
        // We are loading Materials to replace the vanilla assets. This bundle is loaded as-needed for the first time, causing lag.
        // We'll reload it in the menu.
        Debug.Log("Preloading Laggy Asset");
        String strMyAssetBundle = "#@modfolder(Xyth WinterEntities):Resources/WinterMats.unity3d?joewinter";
        DataLoader.LoadAsset<GameObject>(strMyAssetBundle);
    }
}

