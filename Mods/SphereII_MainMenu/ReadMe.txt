The SphereII Mod Localization SDX Script reads the Config/ModLocalization.txt and Config/ModLocalization - Quests.txt at run time, and merges it into the main localization file.

NOTE: Your localization file must be properly formed, with the proper delimiters.