﻿using DMT;
using Harmony;
using System.Reflection;
using UnityEngine;

public class SphereII_EntityPlayerLocal
{
    public class SphereII_EntityPlayerLocal_Start : IHarmony
    {

        public void Start()
        {
            Debug.Log(" Loading Patch: " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }


    // Let's the player crouch and enter into a one block space.
    [HarmonyPatch(typeof(EntityPlayerLocal))]
    [HarmonyPatch("Init")]
    public class SphereII_EntityPlayerLocal_Init
    {
        static void Postfix(int _entityClass, EntityPlayerLocal __instance)
        {
            bool blOneBlockCrouch = false;
            EntityClass entityClass = EntityClass.list[_entityClass];
            if(entityClass.Properties.Values.ContainsKey("OneBlockCrouch"))
                bool.TryParse(entityClass.Properties.Values["OneBlockCrouch"], out blOneBlockCrouch);

            if(blOneBlockCrouch)
            {
                __instance.vp_FPController.PhysicsCrouchHeightModifier = 0.49f;
                __instance.vp_FPController.SyncCharacterController();
            }
        }
    }
 
}



