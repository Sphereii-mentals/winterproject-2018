﻿using DMT;
using Harmony;
using System.Reflection;
using UnityEngine;

public class SphereII_SoftHands
{
    public class SphereII_SoftHands_Start : IHarmony
    {

        public void Start()
        {
            Debug.Log(" Loading Patch: " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }


    // If the player is hitting something with its bare hands, deal damage.
    [HarmonyPatch(typeof(ItemActionDynamic))]
    [HarmonyPatch("hitTarget")]
    public class SphereII_EntityPlayerLocal_IsAttackValid
    {
        public static void Postfix(ItemActionData _actionData, WorldRayHitInfo hitInfo)
        {
            EntityPlayerLocal player = _actionData.invData.holdingEntity as EntityPlayerLocal;
            if(player && _actionData.invData.item.GetItemName() == "meleeHandPlayer")
            {
                WorldRayHitInfo executeActionTarget = player.inventory.holdingItem.Actions[0].GetExecuteActionTarget(player.inventory.holdingItemData.actionData[0]);
                if(executeActionTarget == null)
                    return;

                // If we hit something in our bare hands, get hurt!
                if(executeActionTarget.bHitValid)
                {
                    DamageSource dmg = new DamageSource(EnumDamageSource.Internal, EnumDamageTypes.Bashing);
                    player.DamageEntity(dmg, 1, false, 1f);
                }

            }
        }
    }
}



